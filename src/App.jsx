import { useReducer } from "react";
import { useEffect } from "react";
import Header from "./components/Header";
import MainWrap from "./components/MainWrap";
import Loader from "./components/Loader";
import Error from "./components/Error";
import StartScreen from "./components/StartScreen";
import Qustions from "./components/Qustions";
import NextButton from "./components/NextButton";

const initialState = {
  questions: [],
  status: "loading",
  index: 0,
  answer: null,
};

function reducer(state, action) {
  switch (action.type) {
    case "dataReceived":
      return (state = { ...state, questions: action.payload, status: "ready" });
    case "dataFailed":
      return (state = { ...state, status: "error" });
    case "start":
      return { ...state, status: "active" };
    case "answer":
      return { ...state, answer: action.payload };
    case "newAnswer":
      return { ...state, index: state.index + 1,answer:null };
    default:
      throw new Error("No such action");
  }
}

function App() {
  const [{ questions, status, index, answer }, dispatch] = useReducer(
    reducer,
    initialState
  );

  const numberOfQuestions = questions.length;

  useEffect(() => {
    fetch(`http://localhost:8080/questions`)
      .then((res) => res.json())
      .then((data) => dispatch({ type: "dataReceived", payload: data }))
      .catch((error) => dispatch({ type: "dataFailed" }));
  }, []);
  return (
    <div className="app">
      <Header />
      <MainWrap>
        {status === "loading" && <Loader />}
        {status === "error" && <Error />}
        {status === "ready" && (
          <StartScreen
            numberOfQuestions={numberOfQuestions}
            dispatch={dispatch}
          />
        )}
        {status === "active" && (
          <Qustions
            question={questions[index]}
            dispatch={dispatch}
            answer={answer}
          />
        )}
        {status === "active" && <NextButton dispatch={dispatch}/>}
      </MainWrap>
    </div>
  );
}

export default App;
