import React from "react";
import Options from "./Options";

const Qustions = ({ question, dispatch, answer }) => {
  console.log(question);
  return (
    <div>
      <h4>{question.question}</h4>
      <Options
        options={question.options}
        dispatch={dispatch}
        answer={answer}
        correctOption={question.correctOption}
      />
    </div>
  );
};

export default Qustions;
