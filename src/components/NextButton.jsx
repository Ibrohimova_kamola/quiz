import React from "react";

const NextButton = ({ dispatch }) => {
  return (
    <div className="btn btn-ui" onClick={() => dispatch({ type: "newAnswer" })}>
      Next
    </div>
  );
};

export default NextButton;
