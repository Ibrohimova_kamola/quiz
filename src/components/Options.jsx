import React from "react";

const Options = ({ options, dispatch, answer, correctOption }) => {
  
  return (
    <div className="options">
      {options.map((option, index) => {
        return (
          <div
            className={`btn btn-option ${answer == index && "answer"} ${
              answer ? correctOption == index ? "correct" : "wrong" : ""
            }`}
            onClick={() => dispatch({ type: "answer", payload: index })}
            disabled={answer}
          >
            {option}
          </div>
        );
      })}
    </div>
  );
};

export default Options;
